﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] ParticleSystem deathPrefab;
    [SerializeField] int damage = 2;
    

    public void TakeDamage(float damage)
    {
        Instantiate(deathPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player player = collision.transform.GetComponent<Player>();
        if (player)
        {
            player.TakeDamage(damage, transform.position);
        }
    }

}
