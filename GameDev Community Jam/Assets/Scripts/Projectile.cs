﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] ParticleSystem destroyPrefab;
    [SerializeField] float spinSpeed = 1000;
    [SerializeField] float shootSpeed = 1;
    [SerializeField] Vector2 shootDirection = Vector2.right;
    float damage = 4;


    public void SetUp(bool facingRight, Vector2 _shootDirection)
    {
        if(!facingRight)
        {
            spinSpeed = -spinSpeed;
            shootSpeed = -shootSpeed;
            shootDirection = -_shootDirection;
        }
        else   
            shootDirection = _shootDirection;
    }


    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward, spinSpeed * Time.deltaTime);
        transform.position = Vector2.MoveTowards(transform.position, transform.position + (Vector3)shootDirection, shootSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.GetComponent<Player>())
        {
            Instantiate(destroyPrefab, transform.position, Quaternion.identity);
            if(collision.GetComponent<Enemy>())
            {
                collision.GetComponent<Enemy>().TakeDamage(damage);
            }
            Destroy(gameObject);
        }
    }
}
