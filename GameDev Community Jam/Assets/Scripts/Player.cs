﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [SerializeField] ParticleSystem deathPrefab;
    [SerializeField] LayerMask doorLayer;
    [SerializeField] Projectile projectile;
    [SerializeField] Transform spawnPoint;
    [SerializeField] Texture2D cursor;
    [SerializeField] GameObject displayText;
    [SerializeField] float moveSpeed = 10;
    [SerializeField] float jumpForce;
    [SerializeField] int rayCount = 5;
    [SerializeField] float skinWidth = 0.01f;
    [SerializeField] int extraJumpsValue = 1;

    [SerializeField] float gravityScaleUp, gravityScaleDown;
    int extraJumps;
    bool hurt = false;

    [SerializeField] LayerMask groundLayer;

    PlatformEffector2D[] allDuelWayPlatforms;

    float moveInput;
    bool isGrounded;
    float raySpacing;
    Rigidbody2D rb2D;
    GameController gameController;
    bool dead;
    bool facingRight;

    void Start()
    {
        allDuelWayPlatforms = FindObjectsOfType<PlatformEffector2D>();
        extraJumps = extraJumpsValue;
        rb2D = GetComponent<Rigidbody2D>();
        gameController = GameController.instance;
        facingRight = true;
        Cursor.SetCursor(cursor, new Vector2(16, 16), CursorMode.ForceSoftware);
        CalculateRaySpacing();
    }

    void Update()
    {
        if (!dead)
        {
            Jump();
            Shoot();
            OpenDoor();
        }
    }

    private void OpenDoor()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 1f, doorLayer);
        if (hit)
        {
            displayText.SetActive(true);
            displayText.GetComponent<TMP_Text>().text = "Press E to spend 30 seconds to open the door";
            if (Input.GetKeyDown(KeyCode.E))
            {
                Destroy(hit.transform.gameObject);
            }
        }
        else
        {
            hit = Physics2D.Raycast(transform.position, Vector2.left, 1f, doorLayer);
            if (hit)
            {
                displayText.SetActive(true);
                displayText.GetComponent<TMP_Text>().text = "Press E to spend 30 seconds to open the door";
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Destroy(hit.transform.gameObject);
                }
            }
            else
            {
                displayText.SetActive(false);
            }
        }
    }

    private void Shoot()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 shootDirection = mousePos - (Vector2)transform.position;

        if (transform.position.x - mousePos.x < 0 && !facingRight || transform.position.x - mousePos.x > 0 && facingRight)
        {
            facingRight = !facingRight;
            GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
        }

        if (Input.GetMouseButtonDown(0) && gameController.CanShoot())
        {
            Projectile newProjectile = Instantiate(projectile, transform.position, Quaternion.identity);
            newProjectile.SetUp(facingRight, shootDirection);
        }
    }

    private void Jump()
    {
        if (isGrounded)
        {
            extraJumps = extraJumpsValue;
        }
        if (Input.GetKey(KeyCode.S) && isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            foreach (PlatformEffector2D platform in allDuelWayPlatforms)
            {
                platform.rotationalOffset = 180;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extraJumps > 0)
        {
            rb2D.velocity = Vector2.up * jumpForce;
            extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extraJumps == 0 && isGrounded)
        {
            rb2D.velocity = Vector2.up * jumpForce;
        }

        if (rb2D.velocity.y < 0)
            rb2D.gravityScale = gravityScaleDown;
        else
            rb2D.gravityScale = gravityScaleUp;

        if (Physics2D.OverlapBoxAll(transform.position, transform.GetComponent<BoxCollider2D>().size, 0, groundLayer).Length == 0)
        {
            foreach (PlatformEffector2D platform in allDuelWayPlatforms)
            {
                platform.rotationalOffset = 0;
            }
        }
    }

    void CalculateRaySpacing()
    {
        Bounds bounds = GetComponent<BoxCollider2D>().bounds;


        raySpacing = bounds.size.x / (rayCount - 1);
    }

    private void FixedUpdate()
    {
        if (!dead && !hurt)
        {
            Move();
        }
        else if(dead)
        {
            rb2D.velocity = Vector2.zero;
        }
    }

    private void Move()
    {
        moveInput = Input.GetAxisRaw("Horizontal");
        rb2D.velocity = new Vector2(moveInput * moveSpeed, rb2D.velocity.y);

        //Checking ground
        Bounds bounds = GetComponent<Collider2D>().bounds;
        bounds.Expand(skinWidth * -2);
        isGrounded = false;
        for (int i = 0; i < rayCount; i++)
        {

            Vector2 rayStart = new Vector2(bounds.min.x, bounds.min.y);
            rayStart += Vector2.right * (raySpacing * i);

            RaycastHit2D hit = Physics2D.Raycast(rayStart, Vector2.down, 0.1f, groundLayer);
            if (hit)
                isGrounded = true;

            Debug.DrawRay(rayStart, Vector2.down * 0.1f, Color.red);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + Vector3.right * 1f);
        Gizmos.DrawLine(transform.position, transform.position + Vector3.left * 1f);
    }

    public void Die()
    {
        dead = true;
        Instantiate(deathPrefab, transform.position, Quaternion.identity);
        Color currentColor = GetComponent<SpriteRenderer>().color;
        GetComponent<SpriteRenderer>().color = new Color(currentColor.r, currentColor.g, currentColor.b, 0);
        StartCoroutine(RestartDelay());
    }

    public void TakeDamage(int damage, Vector3 position)
    {
        hurt = true;
        StartCoroutine(HurtCounter());
        gameController.LoseTime(5);

        

        if(transform.position.x - position.x < 0)
        {
            rb2D.velocity = Vector2.zero;
            rb2D.AddForce((transform.position - position + Vector3.left + Vector3.up) * 75, ForceMode2D.Impulse);
        }
        else
        {
            rb2D.velocity = Vector2.zero;
            rb2D.AddForce((transform.position - position + Vector3.right + Vector3.up) * 75, ForceMode2D.Impulse);
        }
    }

    IEnumerator HurtCounter()
    {
        yield return new WaitForSeconds(0.2f);
        hurt = false;
    }

    IEnumerator RestartDelay()
    {
        yield return new WaitForSeconds(2);
        transform.position = spawnPoint.position;
        Color currentColor = GetComponent<SpriteRenderer>().color;
        GetComponent<SpriteRenderer>().color = new Color(currentColor.r, currentColor.g, currentColor.b, 1);
        dead = false;
    }
}
