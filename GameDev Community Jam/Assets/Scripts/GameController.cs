﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    [SerializeField] GameObject bulletUI;
    [SerializeField] Transform bulletHolder;
    [SerializeField] Image timeBar;
    [SerializeField] float maxTime;
    float timeInSeconds;


    [SerializeField] int maxBullets;

    
    List<GameObject> uiBullets = new List<GameObject>();

    private void Awake()
    {
        timeInSeconds = maxTime;
        instance = this;
    }

    private void Start()
    {
        for (int i = 0; i < maxBullets; i++)
        {
            GameObject newBullet = Instantiate(bulletUI, bulletHolder);
            newBullet.transform.parent = bulletHolder;
            uiBullets.Add(newBullet);
        }
    }

    public void LoseTime(float time)
    {
        timeInSeconds -= time;
        Mathf.Clamp(timeInSeconds, 0, maxTime);
        timeBar.fillAmount = timeInSeconds / maxTime;
    }

    public void GainTime(float time)
    {
        timeInSeconds += time;
        Mathf.Clamp(timeInSeconds, 0, maxTime);
        timeBar.fillAmount = timeInSeconds / maxTime;
    }

    public bool CanShoot()
    {
        if(uiBullets.Count > 0)
        {
            Destroy(uiBullets[0]);
            uiBullets.RemoveAt(0);
            return true;
        }
        return false;
    }
}
